﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;

namespace VideoSpelare_InlUppg1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        public MainWindow()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((mediaWindow.Source != null) && (mediaWindow.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = mediaWindow.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = mediaWindow.Position.TotalSeconds;
            }
        }
        // File Dialog
        private void OpenFileBtn_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Media files (*.mp3;*.mpg;*.mpeg;*.mp4;*.avi;)|*.mp3;*.mpg;*.mpeg;*.mp4;*.avi;|All files (*.*)|*.*";
            if (opf.ShowDialog() == true)
                mediaWindow.Source = new Uri(opf.FileName);
            lblMediaName.Text = opf.SafeFileName;
        }

        #region ProgressBar
        private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            mediaWindow.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
        }
        #endregion

        #region MediaButtonEvents
        private void PlayMedia(object sender, RoutedEventArgs e)
        {
            mediaWindow.Play();
            mediaPlayerIsPlaying = true;
        }

        private void PauseMedia(object sender, RoutedEventArgs e)
        {
            mediaWindow.Pause();
        }

        private void StopMedia(object sender, RoutedEventArgs e)
        {
            mediaWindow.Stop();
            mediaPlayerIsPlaying = false;
            sliProgress.Value = mediaWindow.Position.TotalSeconds;
        }
        #endregion
    }
}
